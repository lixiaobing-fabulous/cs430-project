# cs430-project

## 1 Local optimization method

### 1.1 Pseudocode for the algorithm

```python
def separating_points_by_axis_parallel_lines(points):
    number_of_points = len(points)
    x_axis = [point[0] for point in points]
    x_axis.sort()
    y_axis = [point[1] for point in points]
    y_axis.sort()
    x_lines = [("v", (x_axis[i] + x_axis[i + 1]) / 2) for i in range(number_of_points - 1)]
    y_lines = [("h", (y_axis[i] + y_axis[i + 1]) / 2) for i in range(number_of_points - 1)]
    # starting with a feasible solution which is all vertical lines.
    initial_lines = x_lines

    all_possible_lines = x_lines + y_lines
    result = initial_lines
    while result:
        # try to remove all possible 2 lines and replace it with a feasible choice of lines.
        reduce_success = False
        for i in range(len(result) - 1):
            for j in range(i + 1, len(result)):
                for possible_line in all_possible_lines:
                    tmp_lines = [result[k] for k in range(len(result)) if k not in [i, j]]
                    tmp_lines.append(possible_line)
                    if is_valid_answer(points, tmp_lines):
                        result = tmp_lines
                        reduce_success = True
                        break
        if not reduce_success:
            break
    return result

def is_valid_answer(points, lines):
    lines_set = set(lines)
    x_axises = [line[1] for line in lines_set if line[0] == 'v']
    y_axises = [line[1] for line in lines_set if line[0] == 'h']
    x_axises.sort()
    y_axises.sort()

    def find_min_index(x, numbers):
        min_index = -1
        for i in range(len(numbers)):
            if x > numbers[i]:
                min_index = i
            else:
                break
        return min_index

    visited = set()
    for point in points:
        x = point[0]
        y = point[1]
        x_idx = find_min_index(x, x_axises)
        y_idx = find_min_index(y, y_axises)
        section = (x_idx, y_idx)
        if section in visited:
            return False
        visited.add(section)
    return True
```

### 1.2 Analysis of running time

The total running time is O(n^6).

The most time-consuming procedure is the reduction procedure.
There are at most N reduction steps.

For each reduction, We take out all possible two axes from the initial result set. There are N^2 of them.

Then For each, we try all possible lines to see if this new line added can be feasible for this problem. There are 2N possible lines. 

And for each line, we need N^2 time to test if it works.

So, The total running time O(n^6)

### 1.3 One instance fails to return the optimum
For instance05.txt.
```
20
1 9
2 14
3 18
4 15
5 20
6 19
7 5
8 16
9 7
10 4
11 6
12 1
13 10
14 8
15 12
16 13
17 17
18 11
19 2
20 3

```
The run of the algorithm on the instance is below.
```
11
v 15.5
v 17.5
v 19.5
h 9.5
h 15.5
v 3.5
v 5.5
v 7.5
h 4.5
v 10.5
v 13.5
```
There is a better solution below.
```
10
v 10.5
h 9.5
h 16.5
v 15.5
v 5.5
h 6.5
v 3.5
h 11.5
h 4.5
v 19.5
```

## 2 Greedy

### 2.1 Pseudocode for the algorithm
```
# greedy strategy
# 1) find largest groups
# 2) if same groups , find Standard Deviation minimum (make lines balance)
# 3) if same groups and std, use the largest axis number ( make lines seperate, then remaining lines make seperate more groups)
```

```python
from collections import defaultdict
from copy import deepcopy
import math


def greedy(points):
    # type: (list) -> list
    # The second heuristic is the following adaptation of the Greedy algorithm described in Section 35.3 of the textbook.
    # Start with S the empty set.
    # As long as there are pairs of points non-separated,
    # find a line which separates the largest number of pairs of points which were not previously separated.
    # Add this line to S and repeat.
    added_lines = []
    group_to_points = {}
    all_possible_lines, _ = _get_all_possible_lines_and_initial_lines(points)
    while True:
        is_all_separated, new_line, group_to_points = _find_a_line_separates_the_largest(points, added_lines, all_possible_lines, group_to_points)
        added_lines.append(new_line)
        if is_all_separated:
            return added_lines


def _get_all_possible_lines_and_initial_lines(points):
    number_of_points = len(points)
    x_axis = [point[0] for point in points]
    x_axis.sort()
    y_axis = [point[1] for point in points]
    y_axis.sort()
    x_lines = [("v", (x_axis[i] + x_axis[i + 1]) / 2.0) for i in range(number_of_points - 1)]
    y_lines = [("h", (y_axis[i] + y_axis[i + 1]) / 2.0) for i in range(number_of_points - 1)]
    # starting with a feasible solution which is all vertical lines.
    initial_lines = x_lines
    all_possible_lines = x_lines + y_lines
    result = initial_lines
    return all_possible_lines, result


def _find_a_line_separates_the_largest(point_list, added_lines, all_possible_lines, last_group_to_points):
    # 1) find most separated pairs
    # 2) if same pairs , find more groups
    # 3) if same pairs and groups, use the largerest axis number
    result_list = []
    best_separate_pairs, best_group_size, best_line = None, None, None
    found_is_all_separated = False
    for line in all_possible_lines:
        tmp_added_lines = deepcopy(added_lines)
        tmp_added_lines.append(line)
        is_all_separated, group_to_points = _check_all_separated(point_list, tmp_added_lines)
        separate_pairs = _check_separate_pairs(line, last_group_to_points, group_to_points)
        if is_all_separated:
            found_is_all_separated = True
        group_size = len(group_to_points)

        if found_is_all_separated and not is_all_separated:
            # this not meet separated, cancel
            continue

        if not best_separate_pairs:
            best_separate_pairs, best_group_size, best_line, best_group_to_points = separate_pairs, group_size, line, group_to_points
        else:
            if (separate_pairs > best_separate_pairs) \
                    or (separate_pairs == best_separate_pairs and group_size < best_group_size) \
                    or (separate_pairs == best_separate_pairs and group_size == best_group_size and line > best_line):
                best_separate_pairs, best_group_size, best_line, best_group_to_points = separate_pairs, group_size, line, group_to_points

        result_list.append((separate_pairs, group_size, line))
    result_list.sort(reverse=True)

    return found_is_all_separated, best_line, best_group_to_points


def _check_separate_pairs(line, last_group_to_points, group_to_points):
    # calculate separated pairs
    if not last_group_to_points:
        assert len(group_to_points) == 2
        la, lb = group_to_points.values()
        return len(la) * len(lb)
    pairs = 0
    for last_group, points in last_group_to_points.items():
        left_x, right_x, left_y, right_y = last_group
        if last_group not in group_to_points:
            # this group is been separated
            left, right = 0, 0
            if line[0] == 'v':
                x = line[1]
                # 统计x
                for p in points:
                    if p[0] < x:
                        left += 1
                    else:
                        right += 1
            else:
                # 统计y
                y = line[1]
                for p in points:
                    if p[1] < y:
                        left += 1
                    else:
                        right += 1
            pairs += left * right
    return pairs


def _check_all_separated(point_list, line_list):
    x_axises = [line[1] for line in line_list if line[0] == 'v']
    y_axises = [line[1] for line in line_list if line[0] == 'h']
    x_axises.extend([math.inf, -math.inf])
    y_axises.extend([math.inf, -math.inf])
    x_axises.sort()
    y_axises.sort()

    is_all_separated = True

    def find_min_index_left_right(x, numbers):
        for i, number in enumerate(numbers):
            if numbers[i] < x < numbers[i + 1]:
                return i, numbers[i], numbers[i + 1]

    group_to_point_list = defaultdict(list)
    for point in point_list:
        point_x = point[0]
        point_y = point[1]
        x_idx, left_x, right_x = find_min_index_left_right(point_x, x_axises)
        y_idx, left_y, right_y = find_min_index_left_right(point_y, y_axises)
        group = (left_x, right_x, left_y, right_y)
        if group in group_to_point_list:
            is_all_separated = False
        group_to_point_list[group].append(point)

    return is_all_separated, group_to_point_list
```

### 1.2 Analysis of running time

The total running time is O(n^3)

The method _get_all_possible_lines_and_initial_lines is O(N)

The method _find_a_line_separates_the_largest is O(N^3)

    the for loop is O(N)
    
    inside for loop:
    
        the method _check_all_separated is O(N^2) to check separate groups and group size, O(1) to find best solution (greedy strategy)
        the method _check_separate_pairs is O(N^2), outer loop is 2N at most, inner loop is O(N) at most
        
So, The total running time O(N^3)
        

### 2.3 One instance fails to return the optimum
For case instance06 
```
30
1 7
2 28
3 4
4 13
5 14
6 26
7 21
8 5
9 25
10 6
11 19
12 30
13 17
14 18
15 3
16 27
17 11
18 15
19 24
20 1
21 2
22 10
23 12
24 29
25 8
26 16
27 20
28 23
29 9
30 22
```
The run of the algorithm on the instance is below.
```
15
v 15.5
h 15.5
h 23.5
h 9.5
v 8.5
v 21.5
v 27.5
v 11.5
v 4.5
v 17.5
h 17.5
v 29.5
v 22.5
v 20.5
v 2.5
```
There is a better solution below.
```
12
v 4.5
v 8.5
v 11.5
v 14.5
v 17.5
v 20.5
h 10.5
v 23.5
v 27.5
h 22.5
h 4.5
h 17.5
```
##Developers

Xiaobing Li  has completed the local optimization algorithm part.
Mingming Lou has completed the greedy algorithm part.