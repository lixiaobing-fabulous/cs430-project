# coding=utf-8
from collections import defaultdict
from copy import deepcopy
import math
import os.path
import time


# use python3 please!

def separating_points_by_axis_parallel_lines_from_file(from_file, to_file, to_file2):
    points = read_from_file(from_file)
    if not points:
        return

    # for p in points:
    #     print('({},{})'.format(p[0], p[1]))

    start = time.time()
    result = local_optimization(points)
    line_count = len(result)
    end = time.time()

    start2 = time.time()
    result2 = greedy(points)
    line_count2 = len(result2)
    end2 = time.time()

    print("Question: {}\n line_count1: {}\n line_count2: {}\n".format(from_file, line_count, line_count2))
    if line_count != line_count2:
        print(result)
        print("---")
        print(result2)

        # for r in result2:
        #     if r[0]=='v':
        #         print("x={}".format(r[1]))
        #     else:
        #         print("y={}".format(r[1]))

    write_to_file(to_file, result)
    write_to_file(to_file2, result2)
    print("Question: {}\n time1 {}\n time2 {} seconds".format(from_file, str(end - start), str(end2 - start2)))
    print()


def local_optimization(points):
    all_possible_lines, initial_lines = _get_all_possible_lines_and_initial_lines(points)
    result = initial_lines

    while result:
        reduce_success = False
        # try to remove all possible 2 lines and replace it with a feasible choice of lines.
        for i in range(len(result) - 1):
            if reduce_success:
                break
            for j in range(i + 1, len(result)):
                if reduce_success:
                    break
                for possible_line in all_possible_lines:
                    tmp_lines = [result[k] for k in range(len(result)) if k not in [i, j]]
                    tmp_lines.append(possible_line)
                    if _is_valid_answer(points, tmp_lines):
                        result = tmp_lines
                        reduce_success = True
                        break
        if not reduce_success:
            break
    return result


def _get_all_possible_lines_and_initial_lines(points):
    number_of_points = len(points)
    x_axis = [point[0] for point in points]
    x_axis.sort()
    y_axis = [point[1] for point in points]
    y_axis.sort()
    x_lines = [("v", (x_axis[i] + x_axis[i + 1]) / 2.0) for i in range(number_of_points - 1)]
    y_lines = [("h", (y_axis[i] + y_axis[i + 1]) / 2.0) for i in range(number_of_points - 1)]
    # starting with a feasible solution which is all vertical lines.
    initial_lines = x_lines
    all_possible_lines = x_lines + y_lines
    result = initial_lines
    return all_possible_lines, result


def _is_valid_answer(points, lines):  # _check_all_separated
    lines_set = set(lines)
    x_axises = [line[1] for line in lines_set if line[0] == 'v']
    y_axises = [line[1] for line in lines_set if line[0] == 'h']
    x_axises.sort()
    y_axises.sort()

    def find_min_index(x, numbers):
        min_index = -1  # means -inf to the first point
        for i, number in enumerate(numbers):
            if x > number:
                min_index = i
            else:
                break
        return min_index

    visited = set()
    for point in points:
        x = point[0]
        y = point[1]
        x_idx = find_min_index(x, x_axises)
        y_idx = find_min_index(y, y_axises)
        section = (x_idx, y_idx)
        if section in visited:
            return False
        visited.add(section)
    return True


def greedy(points):
    # type: (list) -> list
    # The second heuristic is the following adaptation of the Greedy algorithm described in Section 35.3 of the textbook.
    # Start with S the empty set.
    # As long as there are pairs of points non-separated,
    # find a line which separates the largest number of pairs of points which were not previously separated.
    # Add this line to S and repeat.
    added_lines = []
    group_to_points = {}
    all_possible_lines, _ = _get_all_possible_lines_and_initial_lines(points)
    while True:
        is_all_separated, new_line, group_to_points = _find_a_line_separates_the_largest(points, added_lines, all_possible_lines, group_to_points)
        added_lines.append(new_line)
        if is_all_separated:
            return added_lines


def _find_a_line_separates_the_largest(point_list, added_lines, all_possible_lines, last_group_to_points):
    # 1) find most separated pairs
    # 2) if same pairs , find more groups
    # 3) if same pairs and groups, use the largerest axis number
    result_list = []
    best_separate_pairs, best_group_size, best_line = None, None, None
    found_is_all_separated = False
    for line in all_possible_lines:
        tmp_added_lines = deepcopy(added_lines)
        tmp_added_lines.append(line)
        is_all_separated, group_to_points = _check_all_separated(point_list, tmp_added_lines)
        separate_pairs = _check_separate_pairs(line, last_group_to_points, group_to_points)
        if is_all_separated:
            found_is_all_separated = True
        group_size = len(group_to_points)

        if found_is_all_separated and not is_all_separated:
            # this not meet separated, cancel
            continue

        if not best_separate_pairs:
            best_separate_pairs, best_group_size, best_line, best_group_to_points = separate_pairs, group_size, line, group_to_points
        else:
            if (separate_pairs > best_separate_pairs) \
                    or (separate_pairs == best_separate_pairs and group_size < best_group_size) \
                    or (separate_pairs == best_separate_pairs and group_size == best_group_size and line > best_line):
                best_separate_pairs, best_group_size, best_line, best_group_to_points = separate_pairs, group_size, line, group_to_points

    #     result_list.append((separate_pairs, group_size, line))
    # result_list.sort(reverse=True)

    return found_is_all_separated, best_line, best_group_to_points


def _check_separate_pairs(line, last_group_to_points, group_to_points):
    # calculate separated pairs
    if not last_group_to_points:
        assert len(group_to_points) == 2
        la, lb = group_to_points.values()
        return len(la) * len(lb)
    pairs = 0
    for last_group, points in last_group_to_points.items():
        left_x, right_x, left_y, right_y = last_group
        if last_group not in group_to_points:
            # this group is been separated
            left, right = 0, 0
            if line[0] == 'v':
                x = line[1]
                # 统计x
                for p in points:
                    if p[0] < x:
                        left += 1
                    else:
                        right += 1
            else:
                # 统计y
                y = line[1]
                for p in points:
                    if p[1] < y:
                        left += 1
                    else:
                        right += 1
            pairs += left * right
    return pairs


def _check_all_separated(point_list, line_list):
    x_axises = [line[1] for line in line_list if line[0] == 'v']
    y_axises = [line[1] for line in line_list if line[0] == 'h']
    x_axises.extend([math.inf, -math.inf])
    y_axises.extend([math.inf, -math.inf])
    x_axises.sort()
    y_axises.sort()

    is_all_separated = True

    def find_min_index_left_right(x, numbers):
        for i, number in enumerate(numbers):
            if numbers[i] < x < numbers[i + 1]:
                return i, numbers[i], numbers[i + 1]

    group_to_point_list = defaultdict(list)
    for point in point_list:
        point_x = point[0]
        point_y = point[1]
        x_idx, left_x, right_x = find_min_index_left_right(point_x, x_axises)
        y_idx, left_y, right_y = find_min_index_left_right(point_y, y_axises)
        group = (left_x, right_x, left_y, right_y)
        if group in group_to_point_list:
            is_all_separated = False
        group_to_point_list[group].append(point)

    return is_all_separated, group_to_point_list


def read_from_file(filename):
    if not os.path.isfile(filename):
        return []
    with open(filename) as f:
        s = f.read()

    lines = s.split('\n')
    result = []
    for line in lines:
        if ' ' not in line:
            continue
        a, b = line.split(' ')
        a, b = int(a), int(b)
        result.append((a, b))
    return result


def write_to_file(filename, lines):
    with open(filename, "w") as f:
        f.write(str(len(lines)) + "\n")
        for line in lines:
            f.write(line[0] + ' ' + str(line[1]) + '\n')


if __name__ == '__main__':
    for i in range(0, 100):
        if i < 10:
            input_file_name = "instance0" + str(i) + ".txt"
            output_file_name = "local_solution0" + str(i) + ".txt"
            output_file_name2 = "greedy_solution0" + str(i) + ".txt"
        else:
            input_file_name = "instance" + str(i) + ".txt"
            output_file_name = "local_solution" + str(i) + ".txt"
            output_file_name2 = "greedy_solution" + str(i) + ".txt"
        separating_points_by_axis_parallel_lines_from_file(input_file_name, output_file_name, output_file_name2)
